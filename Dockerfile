# Build Step
FROM maven:3.6.2-jdk-11-slim as builder

# Copy assets to /java/app workdir
COPY src /java/app/src
COPY pom.xml /java/app

# Build the application's jar file
RUN mvn -f /java/app/pom.xml package

# Run Step
FROM openjdk:11-jre-slim

# Copy the built jar file to this container
COPY --from=builder /java/app/target/atlassian-coding-challenge.jar /usr/local/lib/app.jar

# Copy the .env into the container so that it is accessible
# to the java runtime
COPY .env /.env

ENTRYPOINT [ "java", "-jar", "/usr/local/lib/app.jar" ]