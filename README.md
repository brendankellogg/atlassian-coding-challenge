# Atlassian Coding Challenge - Brendan Kellogg

## Overview

This repo is my solution for [Atlassian's SR Full Stack Coding Challenge](https://bitbucket.org/dcor_atlassian/atlassian-code-exercise-sr-full-stack-dev-martech/src/master/).

### Technologies Used

- Java
- MySQL 5.7
- [Undertow Web Server](http://undertow.io/)
- [EBean ORM](https://ebean.io/)

### Reasons for Using These Technologies

- MySQL
    
    This is the relational database that I am the most familiar with. Postgres, or other RDBMSs, would have worked fine
    for this solution as well.

- Undertow
    
    Undertow is a library that makes the Java app itself the actual webserver. Unlike other Java HTTP solutions, Undertow
    does not rely on an external webserver to handle requests such as Apache Tomcat, or NGINX. I have used Undertow once before
    in a lecture I taught at the University of Washington about HTTP microservices. Additionally, languages that I am more familiar
    with, such as Go, set up their webservers themselves so this approach was also more comfortable for me.
    
- Ebean ORM

    I decided to use an ORM because they tend to make applications more maintainable for the future. If you end up needing to change a model, such as splitting the contact's name into distinct `First Name` and `Last Name` field there is much less code to update since ORMs will gracefully handle these changes for you. There won't be tons of SQL statements to update throughout the app.

    I chose `EBean` specifically instead of something like [Hibernate](https://hibernate.org/) because the actual usage of the ORM was quite similar to how `Eloquent` is used in [PHP Laravel](https://laravel.com/docs/5.8/eloquent). `Ebean`, while less popular, still had all of the features that I needed, such as database migrations.

### Hurdles

- Getting set up with Maven
    
    Maven is more complicated than other dependency management tools in other languages, such as NPM, Composer, and go modules. It took me a bit to figure out how to set it up and what it was doing. However, once I got it set up, adding dependencies and using them in the app was a breeze.

- Getting set up with EBean
  
    EBean seemed quite easy to set up while reading their documentation. However, in practice for me it cost me a lot of time on this project. However, like Maven, once I had it setup and working it was extremely easy to use and its API is quite simple, yet powerful.

### Next Steps

If I had more time with this project, these are some of the next steps that I would make to make this app more production ready:

- Automated Tests

    **High Priority**

    **Medium Level of Effort**

    Automated tests are super important to any production app. Being able to be confident about code changes you've made working before you push to production is important so that your users run into as few bugs as possible so they still have confidence in the app and trust it. "Our users will test it on prod" is a testing ideology that leaves a bit to be desired.

    I began creating some tests but ultimately ran into some issues around getting them to work with the Undertow HTTP server. If I had more time to work on this project this is likely the first thing I would start with. However, I wanted to try to adhere to this project's time limit as well and unfortunately ran out of time due to some of the hurdles above before I complete some automated tests.

- Pagination
  
    **High Priority**

    **Medium Level of Effort**
  
    Assuming we don't subscribe to "just add more RAM" idea of scalability, we need a way to limit the number of entities that are retrieved in a single request. If we end up with 1000s of accounts we don't want to return all of them in one `Get All Accounts` request because we would quickly run into issues with resources (and response size) especially if there are many users requesting this information.

    Luckily, EBean [has some support for this](https://ebean.io/docs/query/findPagedList). I would begin by using this and getting the page size (with a maximum of something like 100) and page number (default 1) from request query parameters. e.x. `api/v1/contacts?page=2&pageSize=50` would return results 50-100, assuming they exist.

    Additionally, for user quality of life paginated responses would also return URIs that should be used for getting the previous and next pages, if applicable. This way the server is the source of truth on this and individual clients to do not have to calculate this themselves.

- Caching

    **High Priority**

    **Medium Level of Effort**

    Under high load, this app will make many calls to the database which will slow not only the app down, but anyone else using the same database server. We can mitigate much of this by storing entities in a caching server like [memcached](https://ebean.io/docs/query/findPagedList) that specialize in quick reads. Leaving the database as a sort of backup store of the entities frees it up to handle more from other apps and users.

    The downside of caching is that the entity storage logic of the app will become more complicated. When you add,change, or delete any entity the app will need to update the cache. Ensuring this done without error is extremely important since we could easily ruin the cache's effectiveness if the data inside of it became incorrect. The benefits of the quickness of the cache are quickly outweighed if the data in the cache is unreliable.

- Improved Error Handling
  
    **Medium Priority**

    **Low - Medium Level of Effort**

    There is currently some centralized error handling logic. However, some of the error messages could be more descriptive, especially in cases where the user omitted data in their request that the server needed (such as missing name field when creating a contact). Having better and more descriptive errors will assist the users in usage of the app which will lead to be a better user experience.

    For example, "hey, the email field is required" is much more helpful for the user than ":( something went wrong". The latter might end up with the user adding more load to the customer support agents that could have been avoided just by making error messages a bit better.

    The reason this isn't higher priority is because this issue won't directly impair the app's ability to function at scale like the former two. However, it is still important from a user experience perspective.

- MySQL Connection Retry
  
    **Low Priority**

    **Low Level of Effort**

    Currently, the app has a configurable connection delay for MySQL defined by the `MYSQL_CONNECTION_ATTEMPT_DELAY_MS` `.env` variable. This works, but it could be better with true MySQL connection logic so we wouldn't need to wait this time each time the app started if the MySQL server was already ready to accept connections.

    This problem isn't too big since it really only affects local development, but it would be a nice problem to fix.

### Some Closing Thoughts

This is the first real work in Java that I have done in quite a while (Square eCommerce doesn't tend to use Java) so there were some interesting challenges that building a web server in Java presented for me. These were actually quite fun to solve and iterating on my solution and improving it, making it more readable, more modular, more extensible became a game for me.

Even though Java is not my strongest language, I am actually quite pleased with how the app turned out. It was also fun to see how my experience with creating web apps in other languages such as Go and PHP / Laravel would translate into Java.

Overall, I really enjoyed this. I just wish that I had some more time to add and cleanup some other parts of the app.

## Starting it Up

### Running in Docker With `docker-compose`

This option is likely the easiest since `docker-compose` will do everything for you
like a wizard.

You will need:
- [docker](https://docs.docker.com/docker-for-mac/install/)

- [docker-compose](https://docs.docker.com/compose/install/)

1. Clone this repo
2. Copy the env example: `cp .env.docker.example .env`
3. Ensure the values are correct in the `.env`
4. Build the containers: `docker-compose build`
5. Start the containers: `docker-compose up -d`
   
   Note: The app will not be ready to handle requests for about `15 seconds` (defined by the `MYSQL_CONNECTION_ATTEMPT_DELAY_MS` variable in the `.env`). This is just to give the MySQL server time to start up and be ready to handle connections. If the MySQL server has been running for a while you can safely set this value to `0`. Ideally, given more time I would have liked to implement a true MySQL connection retry solution.

6. The API will not be accessible on `http://localhost:<PORT>`, by default `PORT` will be `4000`.
7. Assuming the port is `4000`, navigate to `http://localhost:4000/api/health`. You should see a response like:
    ```$xslt
     {
         "message": "The app is alive!",
         "status": 200
     }   
    ```

### Running Locally

You will need:
- [maven](https://maven.apache.org/)

- [java jdk (11 preferred)](https://www.oracle.com/technetwork/java/javase/downloads/jdk11-downloads-5066655.html)

0. Clone this repo
1. Run a MySQL 5.7 Server. You can do this with docker, otherwise see the
    [install guide](https://dev.mysql.com/doc/mysql-osx-excerpt/5.7/en/osx-installation.html).
2. Copy the env example: `cp .env.local.example .env`
3. Ensure the `.env` values for MySQL are correct.
4. Package the maven project: `mvn package`
5. Run the `.jar` file (assuming default settings): `java -jar target/atlassian-coding-challenge.jar`
6. The API will not be accessible on `http://localhost:<PORT>`, by default `PORT` will be `4000`.
7. Assuming the port is `4000`, navigate to `http://localhost:4000/api/alive`. You should see a response like:
    ```$xslt
     {
         "message": "The app is alive!",
         "status": 200
     }

## Database Migrations

The app will run these for you when it starts if the `MYSQL_RUN_MIGRATIONS` variable in the `.env` is set to `true`. You can add more migrations by adding additional SQL files to the `src/main/java/resources/migrations` directory. These migrations will be run automatically the next time the app is started.
   
## Usage

You can find a Postman collection in this repo under the `docs` folder. This will give you examples of supported requests. 