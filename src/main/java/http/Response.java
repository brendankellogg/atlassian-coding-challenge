package http;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import io.undertow.server.HttpServerExchange;
import io.undertow.util.Headers;
import io.undertow.util.StatusCodes;
import util.ExcludeAccountFromContactStrategy;
import java.util.HashMap;

public class Response {

    /**
     * Content-Type header value for JSON
     */
    private static final String CONTENT_TYPE_JSON = "application/json; charset=utf8";

    private String message;
    private String route;
    private String method;
    private Object data;
    private int status;
    private Gson jsonEncoder;

    public Response() {
        this.status = StatusCodes.OK;

        this.jsonEncoder = new GsonBuilder()
            .setExclusionStrategies(new ExcludeAccountFromContactStrategy())
            .create();
    }


    /**
     * Sets the message on the Response.
     *
     * @param message String new message value.
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * Sets the route on the Response.
     *
     * @param route String new route value.
     */
    public void setRoute(String route) {
        this.route = route;
    }

    /**
     * Sets the method on the Response.
     *
     * @param method String new method value.
     */
    public void setMethod(String method) {
        this.method = method;
    }


    /**
     * Sets the data on the Response.
     *
     * @param data Object new data value.
     */
    public void setData(Object data) {
        this.data = data;
    }

    /**
     * Gets the Http status of the Response.
     *
     * @return int Http status
     */
    public int getStatus() {
        return this.status;
    }

    /**
     * Sets the Http status on the Response.
     *
     * @param status Object new Http status value.
     */
    public void setStatus(int status) {
        this.status = status;
    }

    /**
     * Sends this response to the given HttpServer exchange.
     *
     * @param exchange HttpServerExchange to send this response to.
     */
    public void sendToExchange(final HttpServerExchange exchange) {
        exchange.getResponseHeaders().add(Headers.CONTENT_TYPE, CONTENT_TYPE_JSON);
        exchange.setStatusCode(this.getStatus());

        HashMap<String, Object> resData = new HashMap<>();
        resData.put("message", this.message);
        resData.put("data", this.data);
        resData.put("status", this.status);
        if (this.route != null) {
            resData.put("route", this.route);
        }
        if (this.method != null) {
            resData.put("method", this.method);
        }

        exchange.getResponseSender().send(this.jsonEncoder.toJson(resData));
    }
}
