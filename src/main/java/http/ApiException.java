package http;

public class ApiException extends RuntimeException {

    private final int statusCode;

    /**
     * Constructor for this ApiException
     *
     * @param statusCode Status code to set.
     * @param message Message to set.
     */
    public ApiException(int statusCode, String message) {
        super(message);
        this.statusCode = statusCode;
    }

    /**
     * Gets the status code for this ApiException
     *
     * @return int Status code for this ApiException
     */
    public int getStatusCode() {
        return this.statusCode;
    }
}
