package http;

import env.AppEnvValues;
import io.undertow.Undertow;
import io.undertow.Undertow.Builder;
import io.undertow.server.HttpHandler;

public class ServerBuilder {

    /**
     * Builds an Undertow instance with the given AppEnvValues and HttpHandler.
     *
     * @param values AppEnvValues to use to configure the server
     * @param handler HttpHandler to use as the main handler
     * @return Undertow configured with the given parameters
     */
    public static Undertow build(AppEnvValues values, HttpHandler handler) {
        Builder builder = Undertow.builder();
        builder.addHttpListener(values.getPortAsInt(), values.getHost());
        builder.setHandler(handler);
        return builder.build();
    }
}
