package http;

import handlers.*;
import io.undertow.server.HttpHandler;
import io.undertow.server.RoutingHandler;
import io.undertow.server.handlers.BlockingHandler;
import io.undertow.server.handlers.PathHandler;

public class Routes {

    /**
     * Not found fallback handler for the app.
     */
    private static final HttpHandler NOT_FOUND_HANDLER = new NotFoundHandler();

    /**
     * Invalid method (method not allowed) handler fallback for the app.
     */
    private static final HttpHandler INVALID_METHOD_HANDLER = new InvalidMethodHandler();

    /**
     * Generates and returns Http routes for the application
     * as an HttpHandler.
     *
     * @throws NoSuchMethodException If any action given to a handler is unsupported.
     * @return HttpHandler Routes for the application
     */
    public static HttpHandler generateRoutes() throws NoSuchMethodException {
        PathHandler mux = new PathHandler();

        mux.addPrefixPath("/api", new PathHandler()
            .addExactPath("/health", new RoutingHandler()
                .get("", new HealthCheckHandler())
                .setFallbackHandler(NOT_FOUND_HANDLER)
                .setInvalidMethodHandler(INVALID_METHOD_HANDLER)
            )
            .addPrefixPath("/v1", new PathHandler()
                .addPrefixPath("/contacts", new RoutingHandler()
                    .get("", new ContactHandler("getAll"))
                    .post("", new ContactHandler("create"))
                    .get("/{id}", new ContactHandler("getSpecific"))
                    .put("/{id}/accounts/{accountId}", new ContactHandler("associateWithAccount"))
                    .setFallbackHandler(NOT_FOUND_HANDLER)
                    .setInvalidMethodHandler(INVALID_METHOD_HANDLER)
                )
                .addPrefixPath("/accounts", new RoutingHandler()
                    .get("", new AccountHandler("getAll"))
                    .post("", new AccountHandler("create"))
                    .get("/{id}", new AccountHandler("getSpecific"))
                    .get("/{id}/contacts", new AccountHandler("getSpecificContacts"))
                    .setFallbackHandler(NOT_FOUND_HANDLER)
                    .setInvalidMethodHandler(INVALID_METHOD_HANDLER)
                )
            )
        );

        mux.addPrefixPath("/", NOT_FOUND_HANDLER);

        return new BlockingHandler(new ErrorHandler(mux));
    }

}
