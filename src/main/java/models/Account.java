package models;

import io.ebean.DB;
import io.undertow.server.HttpServerExchange;
import util.Helpers;
import javax.persistence.*;
import java.util.Deque;
import java.util.List;

@Entity
@Table(name = "accounts")
public class Account extends BaseModel {

    @Column(name = "company_name", nullable = false)
    private String companyName;

    @Column(name = "address_line_one", nullable = false)
    private String addressLineOne;

    @Column(name = "address_line_two")
    private String addressLineTwo;

    @Column(name = "city", nullable = false)
    private String city;

    @Column(name = "state", nullable = false)
    private String state;

    @Column(name = "postal_code", nullable = false)
    private String postalCode;

    @Column(name = "country", nullable = false)
    private String country;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "account")
    private List<Contact> contacts;

    public List<Contact> getContacts() {
        return this.contacts;
    }

    /**
     * Gets the account associated with the "id" param in the given exchange.
     * Returns null if no account exists with the given id.
     *
     * @param exchange HttpServerExchange to get the account from.
     * @return Account account with the the given ID, or null.
     */
    public static Account getFromExchange(final HttpServerExchange exchange) {
        return Account.getFromExchangeInternal(exchange, "id");
    }

    /**
     * Gets the account associated with the param name in the given exchange.
     * Returns null if no account exists with the given id in the given param.
     *
     * @param exchange HttpServerExchange to get the account from.
     * @param paramName String param name to get the account ID from.
     * @return Account account with the the given ID, or null.
     */
    public static Account getFromExchange(final HttpServerExchange exchange, String paramName) {
        return Account.getFromExchangeInternal(exchange, paramName);
    }

    /**
     * Gets the account associated with the param name in the given exchange.
     * Returns null if no account exists with the given id in the given param.
     *
     * @param exchange HttpServerExchange to get the account from.
     * @param paramName String param name to get the account ID from.
     * @return Account account with the the given ID, or null.
     */
    private static Account getFromExchangeInternal(final HttpServerExchange exchange, String paramName) {
        String idStr = "";
        Deque<String> ids = exchange.getQueryParameters().get(paramName);
        if (ids.size() > 0) {
            idStr = ids.getFirst();
        }
        int id = Helpers.parseInt(idStr);

        return DB.find(Account.class, id);
    }
}
