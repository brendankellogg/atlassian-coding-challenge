package models;

import io.ebean.Model;

import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public abstract class BaseModel extends Model {

    @Id
    private long id;

    /**
     * Gets the ID of this Model.
     *
     * @return int ID of the model.
     */
    public long getId() {
        return this.id;
    }
}
