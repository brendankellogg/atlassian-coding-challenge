package models;

import io.ebean.DB;
import io.undertow.server.HttpServerExchange;
import util.Helpers;
import javax.persistence.*;
import java.util.Deque;

@Entity
@Table(name = "contacts")
public class Contact extends BaseModel {

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "email", nullable = false, unique = true)
    private String email;

    @Column(name = "address_line_one", nullable = false)
    private String addressLineOne;

    @Column(name = "address_line_two")
    private String addressLineTwo;

    @Column(name = "city", nullable = false)
    private String city;

    @Column(name = "state", nullable = false)
    private String state;

    @Column(name = "postal_code", nullable = false)
    private String postalCode;

    @Column(name = "country", nullable = false)
    private String country;

    @ManyToOne
    private Account account;

    public void setAccount(Account account) {
        this.account = account;
    }

    /**
     * Gets the contact associated with the "id" param in the given exchange.
     * Returns null if no contact exists with the given id.
     *
     * @param exchange HttpServerExchange to get the contact from.
     * @return Contact contact with the the given ID, or null.
     */
    public static Contact getFromExchange(final HttpServerExchange exchange) {
        String idStr = "";
        Deque<String> ids = exchange.getQueryParameters().get("id");
        if (ids.size() > 0) {
            idStr = ids.getFirst();
        }
        int id = Helpers.parseInt(idStr);

        return DB.find(Contact.class, id);
    }
}
