package env;

import io.github.cdimascio.dotenv.Dotenv;
import util.Helpers;

import java.lang.reflect.Field;

public class AppEnvValues {

    @EnvironmentAnnotation(name="HOST", def = "0.0.0.0")
    public String host;

    @EnvironmentAnnotation(name="PORT", def = "80")
    public String port;

    @EnvironmentAnnotation(name="MYSQL_HOST")
    public String mysqlHost;

    @EnvironmentAnnotation(name="MYSQL_USER", def = "root")
    public String mysqlUser;

    @EnvironmentAnnotation(name="MYSQL_DB", def = "atlassian")
    public String mysqlDB;

    @EnvironmentAnnotation(name="MYSQL_PASS")
    public String mysqlPass;

    @EnvironmentAnnotation(name="MYSQL_RUN_MIGRATIONS", def = "true")
    public String mysqlRunMigrations;

    @EnvironmentAnnotation(name="MYSQL_CONNECTION_ATTEMPT_DELAY_MS=5000", required = false, def = "10000")
    public String mysqlConnectionAttemptDelay;

    /**
     * Creates a new AppEnvValues from the .env file in the
     * project directory.
     *
     * @throws UnsetVariableException If any required variable is unset and has no default value.
     */
    public AppEnvValues() throws UnsetVariableException {
        Dotenv dotenv = Dotenv.load();

        for (Field field : this.getClass().getFields()) {
            EnvironmentAnnotation envDescription = field.getAnnotation(EnvironmentAnnotation.class);

            String value = dotenv.get(envDescription.name(), envDescription.def());
            if (value.length() == 0 && envDescription.required()) {
                throw new UnsetVariableException(envDescription.name());
            }

            try {
                field.set(this, value);
            } catch (IllegalAccessException e) {
                throw new UnsetVariableException(envDescription.name());
            }
        }
    }

    /**
     * Gets the value of the host.
     *
     * @return String Value of the host.
     */
    public String getHost() {
        return this.host;
    }

    /**
     * Gets the value of the port.
     *
     * @return String Value of the port.
     */
    public String getPort() {
        return this.port;
    }

    /**
     * Gets the value of the port as an int.
     *
     * @return int Value of the port.
     */
    public int getPortAsInt() {
        return Integer.parseInt(this.port);
    }

    /**
     * Gets the value of the MySQL host.
     *
     * @return String Value of the MySQL host.
     */
    public String getMysqlHost() {
        return this.mysqlHost;
    }

    /**
     * Gets the value of the MySQL user.
     *
     * @return String Value of the MySQL user.
     */
    public String getMysqlUser() {
        return this.mysqlUser;
    }

    /**
     * Gets the value of the MySQL database.
     *
     * @return String Value of the MySQL database.
     */
    public String getMysqlDB() {
        return this.mysqlDB;
    }

    /**
     * Gets the value of the MySQL password.
     *
     * @return String Value of the MySQL password.
     */
    public String getMysqlPass() {
        return this.mysqlPass;
    }

    /**
     * Gets the value of whether or not MySQL migrations should be run as a boolean.
     *
     * @return boolean Value of whether or not MySQL migrations should be run.
     */
    public boolean shouldRunMigrations() {
        return Boolean.parseBoolean(this.mysqlRunMigrations);
    }

    /**
     * Parses the number of ms to delay the connection attempt as an int
     *
     * @return int MySQL connection attempt delay in MS.
     */
    public int getMysqlConnectionAttemptDelay() {
        int numRetries = Helpers.parseInt(this.mysqlConnectionAttemptDelay);
        if (numRetries < -1) {
            return 0;
        }
        return numRetries;
    }
}
