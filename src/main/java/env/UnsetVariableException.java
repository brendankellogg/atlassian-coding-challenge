package env;

class UnsetVariableException extends RuntimeException {

    /**
     * Creates a new UnsetVariableException for the given variable name.
     *
     * @param variableName Name of the unset variable.
     */
    UnsetVariableException(String variableName) {
        super(String.format("%s was not set and no default was provided", variableName));
    }
}
