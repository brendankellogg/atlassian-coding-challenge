package env;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
@interface EnvironmentAnnotation {
    String name();
    String def() default "";
    boolean required() default true;
}
