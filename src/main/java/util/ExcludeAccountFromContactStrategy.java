package util;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import models.Contact;

/**
 * Used to exclude the account field from the Contact class to
 * prevent Gson trying to serialize a circular reference.
 */
public class ExcludeAccountFromContactStrategy implements ExclusionStrategy {
    @Override
    public boolean shouldSkipField(FieldAttributes fieldAttributes) {
        return fieldAttributes.getDeclaringClass() == Contact.class &&
                fieldAttributes.getName().equals("account");
    }

    @Override
    public boolean shouldSkipClass(Class<?> aClass) {
        return false;
    }
}
