package util;

public class Helpers {

    /**
     * Parses the given string as an int. Returns -1
     * if the given string is not parsable as an int.
     * This exists just as a helper so we don't have to
     * try / catch every time we try to parse an int.
     *
     * @param str String to parse into an int.
     * @return Parsed Int
     */
    public static int parseInt(String str) {
        int result = -1;

        try {
            result = Integer.parseInt(str);
        } catch (Exception e) {
            // no op
        }

        return result;
    }
}
