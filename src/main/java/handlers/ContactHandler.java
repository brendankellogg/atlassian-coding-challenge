package handlers;

import http.ApiException;
import http.Response;
import io.ebean.DB;
import io.ebean.PagedList;
import io.undertow.server.HttpServerExchange;
import io.undertow.util.StatusCodes;
import models.Account;
import models.Contact;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.List;

public class ContactHandler extends BaseJSONHandler {

    /**
     * Creates a new ContactHandler using the given action.
     *
     * @param action String action to call.
     * @throws NoSuchMethodException If the action does not correspond to a declared
     * method.
     */
    public ContactHandler(String action) throws NoSuchMethodException {
        super(action);
    }

    /**
     * Handles Http requests for creating a contact.
     *
     * @param exchange HttpServerExchange Request / Response properties.
     * @return Response Response for the Http request.
     */
    protected Response create(final HttpServerExchange exchange) {
        InputStream stream = exchange.getInputStream();
        Reader reader = new InputStreamReader(stream, StandardCharsets.UTF_8);
        Object newEntity = this.jsonDecoder.fromJson(reader, Contact.class);

        DB.save(newEntity);

        Response response = new Response();
        response.setStatus(StatusCodes.CREATED);
        response.setMessage("Contact saved.");
        response.setData(newEntity);
        return response;
    }

    /**
     * Handles Http requests for getting all contacts.
     *
     * @param exchange HttpServerExchange Request / Response properties.
     * @return Response Response for the Http request.
     */
    protected Response getAll(final HttpServerExchange exchange) {
        List<Contact> contacts = DB.find(Contact.class).findList();

        Response response = new Response();
        response.setMessage("Contacts found.");
        response.setData(contacts);
        return response;
    }

    /**
     * Handles Http requests for getting a specific contact.
     *
     * @param exchange HttpServerExchange Request / Response properties.
     * @return Response Response for the Http request.
     */
    protected Response getSpecific(final HttpServerExchange exchange) {
        Response response = new Response();
        Contact contact = Contact.getFromExchange(exchange);
        if (contact == null) {
            throw new ApiException(StatusCodes.NOT_FOUND, "No contact found.");
        } else {
            response.setMessage("Contact found.");
            response.setData(contact);
        }

        return response;
    }

    /**
     * Handles Http requests for associating a contact with an account
     *
     * @param exchange HttpServerExchange Request / Response properties.
     * @return Response Response for the Http request.
     */
    protected Response associateWithAccount(final HttpServerExchange exchange) throws ApiException {
        Response response = new Response();

        Contact contact = Contact.getFromExchange(exchange);
        if (contact == null) {
            throw new ApiException(StatusCodes.NOT_FOUND, "No contact found.");
        }

        Account account = Account.getFromExchange(exchange, "accountId");
        if (account == null) {
            throw new ApiException(StatusCodes.NOT_FOUND, "No account found.");
        }

        contact.setAccount(account);
        contact.save();

        HashMap<String, Object> responseData = new HashMap<>();
        responseData.put("account", account);
        responseData.put("contact", contact);

        response.setMessage("Contact associated with account");
        response.setData(responseData);
        return response;
    }

}
