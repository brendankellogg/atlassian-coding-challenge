package handlers;

import http.ApiException;
import http.Response;
import io.undertow.server.HttpHandler;
import io.undertow.server.HttpServerExchange;
import io.undertow.util.StatusCodes;

import java.lang.reflect.InvocationTargetException;

public class ErrorHandler implements HttpHandler {

    private static final String DEFAULT_ERROR_MESSAGE = "The server encountered an error processing your request.";

    private HttpHandler next;

    /**
     * Base constructor. Always throws an exception since
     * a "next" HttpHandler is required.
     */
    public ErrorHandler() {
        throw new IllegalArgumentException("an HttpHandler must be provided");
    }

    /**
     * Creates a new ErrorHandler over the given "next" HttpHandler.
     *
     * @param next Next HttpHandler to call.
     */
    public ErrorHandler(HttpHandler next) {
        if (next == null) {
            throw new IllegalArgumentException("the given HttpHandler cannot be null");
        }

        this.next = next;
    }

    /**
     * Invokes the next HttpHandler and censors errors that it throws.
     *
     * @param exchange HttpServerExchange Request / Response properties.
     */
    public void handleRequest(final HttpServerExchange exchange) throws Exception {
        try {
            this.next.handleRequest(exchange);
        } catch (InvocationTargetException e) {
            this.handleInvocationTargetException(e).sendToExchange(exchange);
        } catch (Exception e) {
            Response response = new Response();
            System.out.println(e.getClass());
            System.out.println("Unhandled exception: " + e.getMessage());
            response.setMessage(DEFAULT_ERROR_MESSAGE);
            response.setStatus(StatusCodes.INTERNAL_SERVER_ERROR);
            response.sendToExchange(exchange);
        }
    }

    /**
     * Builds a Response from the given InvocationTargetException
     *
     * @param e InvocationTargetException to build Response from.
     * @return Response built from the given InvocationTargetException
     */
    private Response handleInvocationTargetException(InvocationTargetException e) {
        Response response = new Response();
        if (e.getCause() instanceof ApiException) {
            ApiException apiException = (ApiException) e.getCause();
            response.setStatus(apiException.getStatusCode());
            response.setMessage(apiException.getMessage());
        } else {
            response.setMessage(DEFAULT_ERROR_MESSAGE);
            response.setStatus(StatusCodes.INTERNAL_SERVER_ERROR);
        }
        return response;
    }

}
