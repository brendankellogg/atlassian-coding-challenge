package handlers;

import com.google.gson.Gson;
import http.Response;
import io.undertow.server.HttpHandler;
import io.undertow.server.HttpServerExchange;
import io.undertow.util.Headers;
import io.undertow.util.StatusCodes;

import java.util.HashMap;

public class NotFoundHandler implements HttpHandler {

    private Gson gson;

    public NotFoundHandler() {
        this.gson = new Gson();
    }

    public void handleRequest(final HttpServerExchange exchange) {
        Response response = new Response();

        response.setStatus(StatusCodes.NOT_FOUND);
        response.setMessage("the requested resource was not found");
        response.setRoute(exchange.getRequestURI());

        exchange.getResponseHeaders().add(Headers.CONTENT_TYPE, "application/json");
        exchange.setStatusCode(response.getStatus());
        exchange.getResponseSender().send(this.gson.toJson(response));
    }
}
