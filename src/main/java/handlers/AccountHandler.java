package handlers;

import http.ApiException;
import http.Response;
import io.ebean.DB;
import io.ebean.PagedList;
import io.undertow.server.HttpServerExchange;
import io.undertow.util.StatusCodes;
import models.Account;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.StandardCharsets;
import java.util.List;

public class AccountHandler extends BaseJSONHandler {

    /**
     * Creates a new AccountHandler using the given action.
     *
     * @param action String action to call.
     * @throws NoSuchMethodException If the action does not correspond to a declared
     * method.
     */
    public AccountHandler(String action) throws NoSuchMethodException {
        super(action);
    }

    /**
     * Handles Http requests for creating an account.
     *
     * @param exchange HttpServerExchange Request / Response properties.
     * @return Response Response for the Http request.
     */
    protected Response create(final HttpServerExchange exchange) {
        InputStream stream = exchange.getInputStream();
        Reader reader = new InputStreamReader(stream, StandardCharsets.UTF_8);
        Object newEntity = this.jsonDecoder.fromJson(reader, Account.class);

        DB.save(newEntity);

        Response response = new Response();
        response.setStatus(StatusCodes.CREATED);
        response.setMessage("Account saved.");
        response.setData(newEntity);
        return response;
    }

    /**
     * Handles Http requests for getting all accounts.
     *
     * @param exchange HttpServerExchange Request / Response properties.
     * @return Response Response for the Http request.
     */
    protected Response getAll(final HttpServerExchange exchange) {
        List<Account> accounts = DB.find(Account.class).findList();

        Response response = new Response();
        response.setMessage("Accounts found.");
        response.setData(accounts);
        return response;
    }

    /**
     * Handles Http requests for getting a specific account.
     *
     * @param exchange HttpServerExchange Request / Response properties.
     * @return Response Response for the Http request.
     */
    protected Response getSpecific(final HttpServerExchange exchange) {
        Response response = new Response();
        Account account = Account.getFromExchange(exchange);
        if (account == null) {
            throw new ApiException(StatusCodes.NOT_FOUND, "No account found.");
        } else {
            response.setMessage("Account found.");
            response.setData(account);
        }

        return response;
    }


    /**
     * Handles Http requests for getting a specific account's contacts.
     *
     * @param exchange HttpServerExchange Request / Response properties.
     * @return Response Response for the Http request.
     */
    protected Response getSpecificContacts(final HttpServerExchange exchange) {
        Response response = new Response();
        Account account = Account.getFromExchange(exchange);
        if (account == null) {
            throw new ApiException(StatusCodes.NOT_FOUND, "No account found.");
        } else {
            response.setMessage("Account contacts found.");
            response.setData(account.getContacts());
        }

        return response;
    }

}
