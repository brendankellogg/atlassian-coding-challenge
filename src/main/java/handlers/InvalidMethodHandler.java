package handlers;

import com.google.gson.Gson;
import http.Response;
import io.undertow.server.HttpHandler;
import io.undertow.server.HttpServerExchange;
import io.undertow.util.Headers;
import io.undertow.util.StatusCodes;

public class InvalidMethodHandler implements HttpHandler {

    private Gson gson;

    public InvalidMethodHandler() {
        this.gson = new Gson();
    }

    public void handleRequest(final HttpServerExchange exchange) {
        Response response = new Response();

        response.setStatus(StatusCodes.METHOD_NOT_ALLOWED);
        response.setMessage("the requested route does not support the requested method");
        response.setMethod(exchange.getRequestMethod().toString());
        response.setRoute(exchange.getRequestURI());

        exchange.getResponseHeaders().add(Headers.CONTENT_TYPE, "application/json");
        exchange.setStatusCode(response.getStatus());
        exchange.getResponseSender().send(this.gson.toJson(response));
    }
}
