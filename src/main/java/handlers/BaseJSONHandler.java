package handlers;

import com.google.gson.Gson;
import http.Response;
import io.undertow.server.HttpHandler;
import io.undertow.server.HttpServerExchange;
import java.lang.reflect.Method;

abstract class BaseJSONHandler implements HttpHandler {
    private Method method;
    Gson jsonDecoder;

    BaseJSONHandler(String action) throws NoSuchMethodException {
        this.method = this.getClass().getDeclaredMethod(action, HttpServerExchange.class);
        this.jsonDecoder = new Gson();
    }

    /**
     * Handles Http requests. Invokes the method defined by the action field on this
     * handler.
     *
     *
     * @param exchange HttpServerExchange Request / Response properties.
     * @throws Exception If the action does not map to a declared method, if the
     * handler method does not return an instance of Response, or if the handler
     * method throws its own Exceptions.
     */
    public void handleRequest(final HttpServerExchange exchange) throws Exception {
        Response response;
        try {
            Object handleResponse = this.method.invoke(this, exchange);
            response = (Response) handleResponse;
        } catch (ClassCastException e) {
            throw new IllegalStateException("handler response was not an instance of http.Response");
        }

        response.sendToExchange(exchange);
    }

}
