package handlers;

import http.Response;
import io.undertow.server.HttpServerExchange;
import io.undertow.util.StatusCodes;

public class HealthCheckHandler extends BaseJSONHandler {

    /**
     * Health check method name
     */
    private static final String HEALTH_CHECK_ACTION = "healthCheck";

    /**
     * Creates a HealthCheckHandler that will always invoke
     * healthCheck.
     *
     * @throws NoSuchMethodException If HEALTH_CHECK_ACTION's value does
     * not correlate to a declared method.
     */
    public HealthCheckHandler() throws NoSuchMethodException {
        super(HEALTH_CHECK_ACTION);
    }

    /**
     * Handles Http requests for health checks.
     *
     * @param exchange HttpServerExchange Request / Response properties.
     * @return Response Response for the Http request.
     */
    protected Response healthCheck(final HttpServerExchange exchange) {
        Response response = new Response();
        response.setMessage("The app is alive!");
        response.setStatus(StatusCodes.OK);
        return response;
    }
}
