import database.DatabaseUtil;
import env.AppEnvValues;
import http.Routes;
import http.ServerBuilder;
import io.undertow.Undertow;

public class Server {

    public static void main(String[] args) throws NoSuchMethodException, InterruptedException {
        AppEnvValues appConfig = new AppEnvValues();

        Undertow server = ServerBuilder.build(appConfig, Routes.generateRoutes());

        DatabaseUtil dbUtil = new DatabaseUtil(appConfig);

        // EBean is the ORM that this app uses. This will register the
        // DB information defined in the appConfig above as the default
        // DB connection to use throughout the app.
        dbUtil.setupEBeanDatabase();

        if (appConfig.shouldRunMigrations()) {
            dbUtil.runMigrations();
        }

        System.out.println(String.format("server is listening on %s:%s...\n",
            appConfig.getHost(),
            appConfig.getPort()
        ));

        server.start();
    }
}
