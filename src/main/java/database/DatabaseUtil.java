package database;

import env.AppEnvValues;
import io.ebean.EbeanServer;
import io.ebean.EbeanServerFactory;
import io.ebean.config.ServerConfig;
import io.ebean.migration.MigrationConfig;
import io.ebean.migration.MigrationRunner;

import java.util.Properties;

public class DatabaseUtil {

    private static final String DEFAULT_DATASOURCE = "mysql";
    private static final String DATABASE_DRIVER = "com.mysql.cj.jdbc.Driver";
    private static final String MIGRATIONS_PATH = "migrations";

    private AppEnvValues appConfig;

    public DatabaseUtil(AppEnvValues appConfig) {
        this.appConfig = appConfig;
    }

    /**
     * Registers the DB configuration with EBean for later use using
     * this Util's AppEnvValues.
     *
     * @throws InterruptedException If the database connection is interrupted
     * before it is complete
     */
    public void setupEBeanDatabase() throws InterruptedException {
        ServerConfig cfg = new ServerConfig();
        cfg.setDefaultServer(true);

        Properties properties = new Properties();
        properties.put("ebean.search.packages", "models");
        properties.put("datasource.default", DEFAULT_DATASOURCE);
        properties.put("datasource.db.username", appConfig.getMysqlUser());
        properties.put("datasource.db.password", appConfig.getMysqlPass());
        properties.put("datasource.db.databaseDriver", DATABASE_DRIVER);
        properties.put("datasource.db.databaseUrl", this.getDbURL());

        cfg.loadFromProperties(properties);

        Thread.sleep(appConfig.getMysqlConnectionAttemptDelay());

        EbeanServerFactory.create(cfg);
    }

    /**
     * Runs migrations against the DB defined by this util's
     * AppEnvValues.
     */
    public void runMigrations() {
        MigrationConfig config = new MigrationConfig();
        config.setDbDriver(DATABASE_DRIVER);
        config.setDbUsername(appConfig.getMysqlUser());
        config.setDbPassword(appConfig.getMysqlPass());
        config.setDbUrl(this.getDbURL());
        config.setMigrationPath(MIGRATIONS_PATH);

        MigrationRunner runner = new MigrationRunner(config);
        runner.run();
    }

    /**
     * Gets the DB host URL defined by the util's AppEnvValues
     *
     * @return String DB host URL.
     */
    private String getDbURL() {
        return String.format("jdbc:mysql://%s/%s", appConfig.getMysqlHost(), appConfig.getMysqlDB());
    }
}
