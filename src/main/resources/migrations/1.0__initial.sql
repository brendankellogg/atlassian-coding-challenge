CREATE TABLE accounts (
    id BIGINT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    company_name VARCHAR(50) NOT NULL,
    address_line_one VARCHAR(150) NOT NULL,
    address_line_two VARCHAR(50) NULL DEFAULT NULL,
    city VARCHAR(75) NOT NULL,
    state VARCHAR(20) NOT NULL,
    postal_code VARCHAR(12) NOT NULL,
    country VARCHAR(50) NOT NULL
);

CREATE TABLE contacts (
    id BIGINT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(50) NOT NULL,
    email VARCHAR(100) NOT NULL,
    address_line_one VARCHAR(150) NOT NULL,
    address_line_two VARCHAR(50) NULL DEFAULT NULL,
    city VARCHAR(75) NOT NULL,
    state VARCHAR(20) NOT NULL,
    postal_code VARCHAR(12) NOT NULL,
    country VARCHAR(50) NOT NULL,
    account_id BIGINT UNSIGNED NULL,
    FOREIGN KEY (account_id) REFERENCES accounts(id)
);